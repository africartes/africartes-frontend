module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true,
	},
	extends: "eslint:recommended",
	globals: {
		Atomics: "readonly",
		SharedArrayBuffer: "readonly",
	},
	parser: "babel-eslint",
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: "module",
	},
	rules: {
		"no-extra-semi": "off",
		"no-duplicate-imports": "error",
		"no-unused-vars": ["error", { ignoreRestSiblings: true }],
		"semi-style": ["error", "first"],
	},
	plugins: ["svelte3"],
	overrides: [
		{
			files: "*.svelte",
			processor: "svelte3/svelte3",
		},
	],
}
