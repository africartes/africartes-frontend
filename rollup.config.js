import alias from "@rollup/plugin-alias"
import analyzer from "rollup-plugin-analyzer"
import babel from "@rollup/plugin-babel"
import dotenv from "rollup-plugin-dotenv"
import postcss from "rollup-plugin-postcss"
import replace from "@rollup/plugin-replace"
import sveltePreprocess from "svelte-preprocess"
import { createRollupConfigs } from "./scripts/base.config.js"

const production = !process.env.ROLLUP_WATCH

export const config = {
	staticDir: "static",
	distDir: "dist",
	buildDir: `dist/build`,
	serve: !production,
	production,
	rollupWrapper: (rollup) => {
		rollup.plugins = [
			...rollup.plugins,

			// Allow doing absolute imports like `import xxx from "~/module.js"`, starting from src directory.
			alias({ entries: [{ find: "~", replacement: __dirname + "/src" }] }),

			dotenv(),
			replace({
				"process.env.API_BASE_URL": JSON.stringify(process.env.API_BASE_URL),
				"process.env.APP_TITLE": JSON.stringify(process.env.APP_TITLE),
				"process.env.ENABLED_AREAS": JSON.stringify(process.env.ENABLED_AREAS),
				"process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
			}),
			postcss(),

			// Transpile JavaScript source code for legacy browsers, but penalize modern ones.
			production &&
				babel({
					extensions: [".js", ".mjs", ".html", ".svelte"],
					babelHelpers: "runtime",
					exclude: ["node_modules/@babel/**"],
					presets: [
						[
							"@babel/preset-env",
							{
								targets: "> 0.25%, not dead",
							},
						],
					],
					plugins: [
						"@babel/plugin-syntax-dynamic-import",
						[
							"@babel/plugin-transform-runtime",
							{
								useESModules: true,
							},
						],
					],
				}),

			process.env.ANALYZER && production && analyzer(),
		]
	},
	svelteWrapper: (svelte) => {
		svelte.preprocess = [
			sveltePreprocess({
				// Loads config from postcss.config.js thanks to the postcss-load-config npm module.
				postcss: true,
			}),
		]
	},
}

const configs = createRollupConfigs(config)

export default configs

/**
  Wrappers can either mutate or return a config

  wrapper example 1
  svelteWrapper: (cfg, ctx) => {
    cfg.preprocess: mdsvex({ extension: '.md' }),
  }

  wrapper example 2
  rollupWrapper: cfg => {
    cfg.plugins = [...cfg.plugins, myPlugin()]
    return cfg
  }
*/
