# Africartes frontend

## Install

```bash
git clone https://gitlab.com/africartes/africartes-frontend
cd africartes-frontend
npm install
cp .env.example .env # adapt values in .env if needed
```

## Development

```bash
npm run dev
```

Open up [localhost:5000](http://localhost:5000) and start clicking around. Server reloads on code change.

Note: uses [nollup](https://github.com/PepsRyuu/nollup) for faster rebuilds. To use rollup, run `npm run dev-rollup`, but it's slower since it rebuilds the whole bundle when any file changes.

## Production

Commits to `master` are automatically deployed to production by Netlify.

Note: Sentry is configured in `rollup.config.js`, and enabled in production only.

To test the production build locally:

```bash
npm run build
npm run serve
```

Open up [localhost:5000](http://localhost:5000).
